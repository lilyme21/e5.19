///////////////////////////////////////////////////////////////////
// Lily Mendoza
// CS 49J
// 9/12/19
// This program will prompt and take a users input for a shorthand
// card description and convert it to a full card description.
// The users will continue to be prompted if their input does not
// follow the shorthand card description guidelines.
////////////////////////////////////////////////////////////////////

import java.util.*;


public class E5_19 {
    public static class Card {
        boolean success;
        String value;
        String suit;
        Card() {
            value = "";
            suit = "";
        }
        void setCard() {
            System.out.println("Please enter a shorthand card notation: ");
            Scanner in = new Scanner(System.in);
            String shortHand = in.nextLine();
            if (shortHand.length() == 3) {
                value = shortHand.substring(0,2);
                suit = shortHand.substring(2,3);
            }
            else if(shortHand.length() == 2) {
                value = shortHand.substring(0,1);
                suit = shortHand.substring(1,2);
            }
        }

        public boolean successful() {
            return(success);
        }

        String getDescription()
        {
            String[] values = {"a", "1", "2","3", "4", "5", "6", "7",
                    "8", "9", "10", "j", "q", "k"};
            String[] suits = {"d", "h", "c", "s"};
            int i = 0;
            int indexV = 0,
                    indexS = 0;
            while (i < 14){
                if (value.equalsIgnoreCase(values[i]))
                {
                    indexV = i;
                    success = true;
                    int j = 0;
                    while (j < 4){
                        if (suit.equalsIgnoreCase(suits[j]))
                        {
                            indexS = j;
                            j = 4;
                            success = true;
                        }
                        else {
                            success = false;
                        }
                        j++;
                    };
                    i = 14;
                }
                else {
                    success = false;
                }
                i++;
            };
            String[] cardValues = {"Ace", "1", "2", "3", "4", "5", "6", "7",
                    "8", "9", "10", "Jack", "Queen", "King"};
            String[] cardSuits = {"Diamonds", "Hearts", "Clubs", "Spades"};
            String cardValue = cardValues[indexV],
                    cardSuit = cardSuits[indexS];
            if (successful())
            {
                return(cardValue + " of " + cardSuit);
            }
            else
                return("unknown");

        }
    }
    public static void main(String[] args) {
        // initializing variable to keep track of attempts
        int attempt = 0;
        // initializing boolean variable to stop when successful
        // prompting user to input a shorthand card description
        Card userCard = new Card();
        userCard.setCard();
        System.out.println(userCard.getDescription());
        while(!userCard.successful()){
            System.out.println("Try again.");
            userCard.setCard();
            System.out.println(userCard.getDescription());
            attempt++;
        };
    }
}
